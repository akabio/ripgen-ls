package ripgenls

import (
	"gitlab.com/akabio/ripgen"
)

func hover(doc *document, line, column int) string {
	ast, _ := ripgen.ParseDetailed([]ripgen.Source{{
		Source: []byte(doc.content),
		Name:   "foo",
	}})

	return "AAAA" + ast.FindHover(line, column)
}
