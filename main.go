package ripgenls

import (
	"fmt"
	"os"

	"github.com/tliron/glsp"
	protocol "github.com/tliron/glsp/protocol_3_16"
	"github.com/tliron/glsp/server"

	"github.com/tliron/kutil/logging"
	_ "github.com/tliron/kutil/logging/simple"
)

const lsName = "ripgen"

var (
	version string = "0.0.1"
	handler protocol.Handler
)

func Main() {
	// This increases logging verbosity (optional)
	logging.Configure(1, nil)

	handler = protocol.Handler{
		Initialize:  initialize,
		Initialized: initialized,
		Shutdown:    shutdown,
		SetTrace:    setTrace,
	}

	server := server.NewServer(&handler, lsName, false)

	err := server.RunStdio()
	if err != nil {
		fmt.Println(err)
	}
}

func initialize(context *glsp.Context, params *protocol.InitializeParams) (any, error) {
	docl := &documentListener{
		documents: docs,
	}

	capabilities := handler.CreateServerCapabilities()
	capabilities.HoverProvider = true

	change := protocol.TextDocumentSyncKindFull
	tr := true
	capabilities.TextDocumentSync = protocol.TextDocumentSyncOptions{
		OpenClose: &tr,
		Change:    &change,
		Save:      &tr,
	}

	handler.TextDocumentHover = func(context *glsp.Context, params *protocol.HoverParams) (*protocol.Hover, error) {
		fmt.Fprint(os.Stderr, "hooover", params.TextDocument.URI, params.Position)

		doc := docl.documents.get(params.TextDocument.URI)
		value := hover(doc, int(params.Position.Line), int(params.Position.Character))

		return &protocol.Hover{
			Contents: protocol.MarkupContent{
				Kind:  protocol.MarkupKindMarkdown,
				Value: value,
			},
		}, nil
	}

	handler.TextDocumentDidOpen = docl.didOpen
	handler.TextDocumentDidChange = docl.didChange
	handler.TextDocumentDidSave = docl.didSave

	fmt.Fprintln(os.Stderr, "initialized")

	return protocol.InitializeResult{
		Capabilities: capabilities,
		ServerInfo: &protocol.InitializeResultServerInfo{
			Name:    lsName,
			Version: &version,
		},
	}, nil
}

func initialized(context *glsp.Context, params *protocol.InitializedParams) error {
	return nil
}

func shutdown(context *glsp.Context) error {
	protocol.SetTraceValue(protocol.TraceValueOff)
	return nil
}

func setTrace(context *glsp.Context, params *protocol.SetTraceParams) error {
	protocol.SetTraceValue(params.Value)
	return nil
}
