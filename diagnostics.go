package ripgenls

import (
	"errors"

	protocol "github.com/tliron/glsp/protocol_3_16"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/model"
)

func diagnose(doc *document) []protocol.Diagnostic {
	_, errs := ripgen.ParseDetailed([]ripgen.Source{{
		Source: []byte(doc.content),
		Name:   "foo",
	}})

	diagnostics := []protocol.Diagnostic{}

	for _, pe := range errs {

		position := protocol.Position{}
		positionTo := protocol.Position{}

		var loc model.Location
		hasLoc := errors.As(pe, &loc)

		if hasLoc {
			cf, ct := loc.Column()
			lf, lt := loc.Line()
			position = protocol.Position{
				Line:      protocol.UInteger(lf),
				Character: protocol.UInteger(cf),
			}
			positionTo = protocol.Position{
				Line:      protocol.UInteger(lt),
				Character: protocol.UInteger(ct),
			}
		}

		severity := protocol.DiagnosticSeverityError
		diagnostics = append(diagnostics, protocol.Diagnostic{
			Range: protocol.Range{
				Start: position,
				End:   positionTo,
			},
			Severity: &severity,
			Message:  pe.Error(),
		})
	}

	return diagnostics
}
