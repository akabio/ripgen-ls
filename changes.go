package ripgenls

import (
	"fmt"
	"os"

	"github.com/tliron/glsp"
	protocol "github.com/tliron/glsp/protocol_3_16"
)

type documentListener struct {
	documents *documents
}

func (d *documentListener) didOpen(context *glsp.Context, params *protocol.DidOpenTextDocumentParams) error {
	err := d.documents.didOpen(params)
	if err != nil {
		return err
	}

	doc := d.documents.get(params.TextDocument.URI)
	sendDiagnostics(context, doc)

	return nil
}

func (d *documentListener) didChange(context *glsp.Context, params *protocol.DidChangeTextDocumentParams) error {
	err := d.documents.didChange(params)
	if err != nil {
		return err
	}

	doc := d.documents.get(params.TextDocument.URI)
	sendDiagnostics(context, doc)

	return nil
}

func (d *documentListener) didSave(context *glsp.Context, params *protocol.DidSaveTextDocumentParams) error {
	fmt.Fprintln(os.Stderr, "did save", params.TextDocument.URI)
	return nil
}

func sendDiagnostics(context *glsp.Context, doc *document) {
	if doc != nil {
		go context.Notify(protocol.ServerTextDocumentPublishDiagnostics, &protocol.PublishDiagnosticsParams{
			URI:         doc.uri,
			Diagnostics: diagnose(doc),
		})
	}
}
