package ripgenls

import (
	"fmt"
	"os"

	protocol "github.com/tliron/glsp/protocol_3_16"
)

var docs = &documents{
	documents: map[string]*document{},
}

type documents struct {
	documents map[string]*document
}

type document struct {
	content string
	uri     string
}

func (d *documents) get(uri string) *document {
	return d.documents[uri]
}

func (d *documents) didOpen(params *protocol.DidOpenTextDocumentParams) error {
	fmt.Fprintln(os.Stderr, "did open", params.TextDocument.URI, params.TextDocument.Version)

	doc := d.documents[params.TextDocument.URI]
	if doc == nil {
		doc = &document{
			uri: params.TextDocument.URI,
		}
		d.documents[params.TextDocument.URI] = doc
	}

	doc.content = params.TextDocument.Text

	return nil
}

func (d *documents) didChange(params *protocol.DidChangeTextDocumentParams) error {
	fmt.Fprintln(os.Stderr, "did chg", params.TextDocument.URI, params.TextDocument.Version)

	doc := d.documents[params.TextDocument.URI]
	if doc == nil {
		doc = &document{
			uri: params.TextDocument.URI,
		}
		d.documents[params.TextDocument.URI] = doc
	}

	change := params.ContentChanges[0].(protocol.TextDocumentContentChangeEventWhole)
	doc.content = change.Text

	return nil
}
